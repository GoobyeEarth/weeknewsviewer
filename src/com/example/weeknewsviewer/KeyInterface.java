package com.example.weeknewsviewer;

public interface KeyInterface {
	 String WORKSPACE = "/storage/emulated/0/Download/";
	 
	 //preff
	 String PREFERENCE_FILE = "preference_file";
	 
	 String PREF_CURRENT_DATE = "current_date";
	 String PREF_CURRENT_DATE_FOR_SELECTED = "current_date_selected";
	 String PREF_DATABASE= "database";
	 String PREF_TABLE_NAME = "table";
	 
	 String PREF_START_DATE = "start_date";
	 String PREF_END_DATE = "end_date";
	 String PREF_SCROLL_Y = "scroll_y";
	 
	 int TEXT_SIZE = 17;
}
