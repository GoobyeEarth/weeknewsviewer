package com.example.weeknewsviewer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.example.weeknewsviewer.parts.DetailsDialogClass;
import com.example.weeknewsviewer.parts.NewsDaoClass;
import com.example.weeknewsviewer.parts.NewsDataClass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import library.widget.ListDialogClass;
import library.widget.ListViewClass;
import library.widget.ListViewClass.ChildViewListener;
import library.widget.ListViewClass.ItemClickListener;
import library.widget.PreferenceClass;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class SelectedViewerActivity extends AbstractWeekViewerActivity implements KeyInterface{

	@Override
	protected List<NewsDataClass> getData(String date) {
		
		return dao.getDatasWhere("check_num=1 and date='" + date + "'");
		
	}

	@Override
	protected Class<? extends AbstractWeekViewerActivity> getWeekViewer() {
		
		return SelectedViewerActivity.class;
	}

	@Override
	protected String getPrefDate() {
		// TODO Auto-generated method stub
		return PREF_CURRENT_DATE_FOR_SELECTED;
	}

	@Override
	protected int periodValue() {
		
		return 1;
	}

	@Override
	protected int periodField() {
		// TODO Auto-generated method stub
		return Calendar.MONTH;
	}
	
	
	
	

}