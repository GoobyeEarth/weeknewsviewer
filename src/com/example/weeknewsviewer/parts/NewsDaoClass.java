package com.example.weeknewsviewer.parts;
import java.util.ArrayList;
import java.util.List;

import com.example.weeknewsviewer.KeyInterface;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import library.widget.DaoAbstractClass;


public class NewsDaoClass extends DaoAbstractClass implements KeyInterface{
	private String DATABASE_NAME = WORKSPACE + PREF_DATABASE;
	private String TABLE_NAME = "row";
	public static final String TABLE_ROW = "row";
	public static final String TABLE_EXTRACTED = "extract_other_countries";
	
	private static final String COLUMN_DATE = "date";
	private static final String COLUMN_DOWEEK = "day_of_week";
	private static final String COLUMN_TIME = "time";
	private static final String COLUMN_TIMESTAMP = "timestamp";
	private static final String COLUMN_TITLE = "title";
	private static final String COLUMN_URL = "url";
	private static final String COLUMN_DETAILS = "details";
	private static final String COLUMN_CHECK = "check_num";
	

	private static final int NUM_DATE = 0;
	private static final int NUM_DOWEEK = 1;
	private static final int NUM_TIME = 2;
	private static final int NUM_TIMESTAMP = 3;
	private static final int NUM_TITLE = 4;
	private static final int NUM_URL = 5;
	private static final int NUM_DETAILS = 6;
	private static final int NUM_CHECK = 7;
	
	private static final String[] COLUMN = new String[]{
		COLUMN_DATE,
		COLUMN_DOWEEK,
		COLUMN_TIME,
		COLUMN_TIMESTAMP,
		COLUMN_TITLE,
		COLUMN_URL,
		COLUMN_DETAILS,
		COLUMN_CHECK
	};

	private static final int[] TYPES = new int[]{
		TYPE_STRING,
		TYPE_STRING,
		TYPE_STRING,
		TYPE_STRING,
		TYPE_STRING,
		TYPE_STRING,
		TYPE_STRING,
		TYPE_INT
	};
	
	public NewsDaoClass(Context context, String databaseName, String tableName){
		super(context, WORKSPACE + databaseName, tableName, COLUMN, TYPES);
		TABLE_NAME = tableName;
		DATABASE_NAME = WORKSPACE + databaseName;
	}
	
	/**
	 * 全データの取得 ----------------①
	 *
	 * @return
	 */
	public List<NewsDataClass> findAll(){
		List<NewsDataClass> entityList = new ArrayList<NewsDataClass>();

		Cursor cursor = db.query(TABLE_NAME, column, null, null, null, null, COLUMN_TIMESTAMP);

		while (cursor.moveToNext()) {
			entityList.add(getDataClass(cursor) );
		}

		return entityList;
	}
	
	
	public List<NewsDataClass> findChecked(){
		List<NewsDataClass> entityList = new ArrayList<NewsDataClass>();
		String selection = "check_num = 1"; 
		Cursor cursor = db.query(TABLE_NAME, column, selection, null, null, null, COLUMN_TIMESTAMP);

		while (cursor.moveToNext()) {
			entityList.add(getDataClass(cursor) );
		}

		return entityList;
	}
	
	

//	/** not used
//	 * 特定IDのデータを取得 ----------------②
//	 *
//	 * @param rowId
//	 * @return
//	 */
//	public NewsDataClass findById(int rowId){
//		String selection = COLUMN_ID + "=" + rowId;
//		Cursor cursor = db.query(TABLE_NAME, column, selection, null, null,
//				null, null);
//
//		/*
//		 * to start id with 1.
//		 */
//		if(cursor.moveToNext() ){
//			return getDataClass(cursor);
//		}
//		else{
//			NewsDataClass entity = new NewsDataClass();
//			entity.id = -1;
//			return entity;
//		}
//
//	}

	private NewsDataClass getDataClass(Cursor cursor){
		NewsDataClass entity = new NewsDataClass();
		entity.date = cursor.getString(NUM_DATE);
		entity.doWeek = cursor.getString(NUM_DOWEEK);
		entity.time = cursor.getString(NUM_TIME);
		entity.timeStamp = cursor.getString(NUM_TIMESTAMP);
		entity.title = cursor.getString(NUM_TITLE);
		entity.url = cursor.getString(NUM_URL);
		entity.details = cursor.getString(NUM_DETAILS);
		entity.check = cursor.getInt(NUM_CHECK);


		return entity;
	}

	/**
	 * データの登録 ----------------③
	 *
	 * @param data
	 * @return
	 */
	public String insert(NewsDataClass entity){

		db.insert(TABLE_NAME, null, getValues(entity) );
		return entity.timeStamp;
	}
	
	/**
	 * データの登録
	 * 複数
	 * @param entityList
	 */
	public void insert(List<NewsDataClass> entityList){
		for(int i=0; i < entityList.size(); i++){
				insert( entityList.get(i) );
		}

	}

	/**
	 * データの更新 ----------------④
	 *
	 * @param rowid
	 * @param date
	 * @return
	 */
	public int update(NewsDataClass entity) {
		String whereClause = COLUMN_TIMESTAMP + "=" + "'" +entity.timeStamp + "'";
		return db.update(TABLE_NAME, getValues(entity), whereClause, null);
	}

	private ContentValues getValues(NewsDataClass entity){
		ContentValues values = new ContentValues();

		
		values.put(COLUMN_DATE, entity.date);
		values.put(COLUMN_DOWEEK, entity.doWeek);
		values.put(COLUMN_TIME, entity.time);
		values.put(COLUMN_TIMESTAMP, entity.timeStamp);
		values.put(COLUMN_TITLE, entity.title);
		values.put(COLUMN_URL, entity.url);
		values.put(COLUMN_DETAILS, entity.details);
		values.put(COLUMN_CHECK, entity.check);
		
		return values;

	}

//	/**
//	 * データの削除 ----------------⑤
//	 *
//	 * @param id
//	 * @return
//	 */
//	public int delete(int id) {
//		String whereClause = COLUMN_DATE + "=" + id;
//		return db.delete(TABLE_NAME, whereClause, null);
//	}


	/**
	 * データ数
	 * @return
	 */
   public long getRecordCount() {
       String sql = "select count(*) from " + TABLE_NAME;
       Cursor c = db.rawQuery(sql, null);
       c.moveToLast();
       long count = c.getLong(0);
       c.close();
       return count;
   }
   
   public List<NewsDataClass> getDatasWhere(String selection){
	   List<NewsDataClass> entityList = new ArrayList<NewsDataClass>();
	   Cursor cursor = db.query(TABLE_NAME, column, selection, null, null,
				null, null);

		while (cursor.moveToNext()) {
			entityList.add(getDataClass(cursor) );
		}

		return entityList;
   }
   
   public List<NewsDataClass> getDatasByDate(String date){
	   String selection = COLUMN_DATE + "=" + "'" + date + "'";
	   List<NewsDataClass> entityList = new ArrayList<NewsDataClass>();
	   Cursor cursor = db.query(TABLE_NAME, column, selection, null, null,
				null, null);

		while (cursor.moveToNext()) {
			entityList.add(getDataClass(cursor) );
		}

		return entityList;
   }
   
//	/**
//	 * 特定のidのデータを取得
//	 * @param id
//	 * @return
//	 */
//	public List<NewsDataClass> getDatasByViewId(int id){
//		String selection = COLUMN_ID + "=" + id;
//		List<NewsDataClass> entityList = new ArrayList<NewsDataClass>();
//		Cursor cursor = db.query(TABLE_NAME, column, selection, null, null,
//				null, null);
//
//		while (cursor.moveToNext()) {
//			entityList.add(getDataClass(cursor) );
//		}
//
//		return entityList;
//	}
//	
//	/**
//	 * 特定のidのデータを取得
//	 * @param id
//	 * @return
//	 */
//	public List<NewsDataClass> getDatasByIdOrder(int id, String orderBy){
//		String selection = COLUMN_ID + "=" + id;
//		List<NewsDataClass> entityList = new ArrayList<NewsDataClass>();
//		Cursor cursor = db.query(TABLE_NAME, column, selection, null, null,
//				null, orderBy);
//
//		while (cursor.moveToNext()) {
//			entityList.add(getDataClass(cursor) );
//		}
//
//		return entityList;
//	}
//	
//	/**
//	 * 特定のidのデータを取得
//	 * @param id
//	 * @return
//	 */
//	public NewsDataClass getDataByIdOrder(int id, String orderBy, int order){
//		String selection = COLUMN_ID + "=" + id;
//		
//		Cursor cursor = db.query(TABLE_NAME, column, selection, null, null, null, orderBy);
//		
//		boolean isData = false
//				;
//		for(int i = 0; i < order; i++) {
//			isData = cursor.moveToNext();
//		}
//		
//		if(isData){
//			return getDataClass(cursor);
//		}
//		else{
//			NewsDataClass history = new NewsDataClass();
//			history.id = -1;
//			return history;
//		}
//		
//	}
	
	public boolean getEntityExists(String column, String sig, String right){
		  String sql = "select count(*) from " + TABLE_NAME + " WHERE " + column + sig + "'" + right + "'"; 
	      Cursor c = db.rawQuery(sql, null);
	      c.moveToLast();
	      long count = c.getLong(0);
	      c.close();
	      if(count >= 1){
	    	  return true;
	      }
	      else{
	    	  return false;
	      }
	  }
	
	
}
