package com.example.weeknewsviewer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.example.weeknewsviewer.parts.*;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Layout;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import library.widget.DaoInterface;
import library.widget.FragmentDialogClass;
import library.widget.ListDialogClass;
import library.widget.PreferenceClass;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class ViewerActivity extends AbstractWeekViewerActivity implements KeyInterface{

	@Override
	protected List<NewsDataClass> getData(String date) {
		
		return dao.getDatasByDate(date);
	}

	@Override
	protected Class<? extends AbstractWeekViewerActivity> getWeekViewer() {
		
		return ViewerActivity.class;
	}

	@Override
	protected String getPrefDate() {
		return PREF_CURRENT_DATE;
	}

	@Override
	protected int periodValue() {
		// TODO Auto-generated method stub
		return 7;
	}

	@Override
	protected int periodField() {
		// TODO Auto-generated method stub
		return Calendar.DAY_OF_MONTH;
	}
	
	
	
	
	
}