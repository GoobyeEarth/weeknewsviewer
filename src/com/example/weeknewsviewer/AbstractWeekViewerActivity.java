package com.example.weeknewsviewer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.ExecutionException;

import com.example.weeknewsviewer.parts.*;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.text.Layout;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import library.widget.DaoInterface;
import library.widget.FragmentDialogClass;
import library.widget.ListDialogClass;
import library.widget.PreferenceClass;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

abstract public class AbstractWeekViewerActivity extends Activity implements KeyInterface{
	private Activity  activity= this;
	protected Context context = this;
	private Class<? extends AbstractWeekViewerActivity> viewerActivity;
	
	
	//view class
	private ScrollView mainScrollView;
	private LinearLayout linearLayout;
	private List<CheckBox> titleCheckBoxes = new ArrayList<CheckBox>();
	private List<ImageButton> detailsButtons = new ArrayList<ImageButton>();
	
	private LinearLayout headerLinear;
	private Button commentButton;
	private Button scrollButton;
	
	private LinearLayout underLinear;
	private ImageButton backButton;
	private ImageButton goButton;
	private Button dateButton;
	
	//data
	protected List<NewsDataClass> data;
	protected NewsDaoClass dao;
	private PreferenceClass pref;
	private String dateStr;
	
	private CommentDataClass comment;
	private CommentDaoClass commentDao;
	
	private ProgressDaoClass progressDao;
	private ProgressDataClass progress;
	private boolean hasFinished;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controller();

		mainview();
		
		listener();
	}
	
	abstract protected List<NewsDataClass> getData(String date); 
	
	abstract protected Class<? extends AbstractWeekViewerActivity> getWeekViewer();
	
	abstract protected String getPrefDate();
	
	abstract protected int periodValue();
	
	abstract protected int periodField();
	
	
	private void controller(){
		pref = new PreferenceClass(context, PREFERENCE_FILE);
		dateStr = pref.loadString(getPrefDate());
		
		String db = pref.loadString(PREF_DATABASE);
		String table = pref.loadString(PREF_TABLE_NAME);
		dao = new NewsDaoClass(context, db, table);
//		data = dao.getDatasByDate(dateStr);
		dataToList();
		
		commentDao = new CommentDaoClass(context, db);
		comment = commentDao.findByDate(dateStr);
		
		progressDao = new ProgressDaoClass(context, db);
		progress = progressDao.findByDate(dateStr);
		hasFinished = progress.archiveDate != null;
		
		viewerActivity = getWeekViewer();
		
		Toast.makeText(context, hasFinished + "", Toast.LENGTH_SHORT).show();
		
		
	}
	
	
	private void dataToList(){
		String cDateStr = dateStr;
		
		Calendar cal = Calendar.getInstance();
		cal = setDate(cDateStr);
		
		cal.add(periodField(), periodValue());
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int date = cal.get(Calendar.DAY_OF_MONTH);
		
		String goalDateStr = year + "/" + month + "/" + date;
		
		
		data = new ArrayList<NewsDataClass>();
		
		for(; !cDateStr.equals(goalDateStr); ) {
			data.addAll(getData(cDateStr));
			cDateStr = dateChange(cDateStr, 1);
			
		}
		
		
	}
	
	private void mainview(){
		mainScrollView = new ScrollView(context);
		linearLayout = new LinearLayout(context);
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		headerLinear = new LinearLayout(context);
		headerLinear.setOrientation(LinearLayout.HORIZONTAL);
		commentButton = new Button(context);
		scrollButton = new Button(context);
		underLinear = new LinearLayout(context);
		underLinear.setOrientation(LinearLayout.HORIZONTAL);
		backButton = new ImageButton(context);
		goButton = new ImageButton(context);
		dateButton = new Button(context);
		
		//places
		
		setContentView(mainScrollView);
		mainScrollView.addView(linearLayout);
		
		LayoutParams layoutParams = new  LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		
		linearLayout.addView(headerLinear, layoutParams);
		LayoutParams cbLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1);
		headerLinear.addView(commentButton, cbLP);
		LayoutParams sbLP = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		headerLinear.addView(scrollButton, sbLP);
		
		for(int i = 0; i < data.size(); i++) {
//			if(0 == i || !data.get(i).doWeek.equals(data.get(i-1).doWeek)){
			if(0 == i || !data.get(i).date.equals(data.get(i-1).date)){
				
				TextView textView = new TextView(context);
				textView.setTextSize(14);
				textView.setTextColor(Color.BLACK);
				textView.setTypeface(null, Typeface.BOLD);
				textView.setText(data.get(i).doWeek + " : " + data.get(i).date);
				linearLayout.addView(textView);
				textView.setBackgroundColor(Color.parseColor("#d2ffd2"));
			}
			
			LinearLayout rowLinear = new LinearLayout(context);
			
			rowLinear.setOrientation(LinearLayout.HORIZONTAL);
			linearLayout.addView(rowLinear);
			
			CheckBox titleCheck = new CheckBox(context);
			titleCheckBoxes.add(titleCheck);
			LinearLayout.LayoutParams titleLP = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1);
			rowLinear.addView(titleCheck, titleLP);
			
			
			
			
			ImageButton detailsButton = new ImageButton(context);
			LinearLayout.LayoutParams detailsLP = new LinearLayout.LayoutParams(90, LayoutParams.WRAP_CONTENT);
			
			detailsButtons.add(detailsButton);
			rowLinear.addView(detailsButton, detailsLP);
			
			
			
			
			
		}
		
		linearLayout.addView(underLinear);
		
		LinearLayout.LayoutParams backButtonLP = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		underLinear.addView(backButton, backButtonLP);

		LinearLayout.LayoutParams dateButtonLP = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT, 1);
		underLinear.addView(dateButton, dateButtonLP);
		
		LinearLayout.LayoutParams goButtonLP = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
		underLinear.addView(goButton, goButtonLP);

		
//		picture and text
		String commentText;
		if(hasFinished) {
			commentText = hasFinished + " : " + comment.comment;
		}
		else{
			commentText = comment.comment;
		}
		commentButton.setText(commentText);
		
		for(int i = 0; i < data.size(); i++) {
			CheckBox titleCheck  = titleCheckBoxes.get(i);
			titleCheck.setTextSize(TEXT_SIZE);
			if(data.get(i).check == 1) titleCheck.setChecked(true);
			else titleCheck.setChecked(false);
			
			if(i %2 == 1){
				int white = 255;
				titleCheck.setBackgroundColor(Color.rgb(white, white, white));
			}
			else{
				int grey = 255 - 20;
				titleCheck.setBackgroundColor(Color.rgb(grey, grey, grey));
			}
			titleCheck.setText(data.get(i).title);
			
			ImageButton detailsButton = detailsButtons.get(i);
			detailsButton.setImageResource(R.drawable.ic_details_black_24dp);
			
		}
		scrollButton.setText("scroll");
		
		backButton.setImageResource(R.drawable.ic_keyboard_arrow_left_black_36dp);
		
		String dowStr = getDayOfWeekStr(dateStr);
		
		dateButton.setText(dateStr + "  :" + dowStr);
		
		
		goButton.setImageResource(R.drawable.ic_keyboard_arrow_right_black_36dp);
		
	}
	
	protected String getDayOfWeekStr(String date){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		try {
			cal.setTime(sdf.parse(date));
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		int dayOfWeekNum = cal.get(Calendar.DAY_OF_WEEK);
		if(dayOfWeekNum == Calendar.SUNDAY) return "日";
		if(dayOfWeekNum == Calendar.MONDAY) return "月";
		if(dayOfWeekNum == Calendar.TUESDAY) return "火";
		if(dayOfWeekNum == Calendar.WEDNESDAY) return "水";
		if(dayOfWeekNum == Calendar.THURSDAY) return "木";
		if(dayOfWeekNum == Calendar.FRIDAY) return "金";
		if(dayOfWeekNum == Calendar.SATURDAY) return "土";
		return "error";
	}
	
	private void listener() {
		commentButton.setOnClickListener(new Button.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final EditText editText = new EditText(context);
				editText.setText(comment.comment);
				FragmentDialogClass dialog = new FragmentDialogClass(editText, activity);
				dialog.show("comment", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						comment.comment = editText.getText().toString();
						comment.date = dateStr;
						Toast.makeText(context, comment.comment, Toast.LENGTH_SHORT).show();
						commentDao.saveComment(comment);
						commentButton.setText(comment.comment);
					}
				});
			}
		});
		
		commentButton.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				List<CommentDataClass> comments = commentDao.findAll();
				ListDialogClass dialog = new ListDialogClass(activity, "comments");
				for (final CommentDataClass comment: comments) {
					dialog.addItem(comment.comment + " - " + comment.date, new ListDialogClass.ClickListener() {
						
						@Override
						public void setProcess(int num, String str) {
							dateStr = comment.date;
							pref.saveString(getPrefDate(), dateStr);
							Toast.makeText(context, dateStr, Toast.LENGTH_SHORT).show();
							Intent intent = new Intent(context, viewerActivity);
							startActivity(intent);
							activity.finish();
						}
					});
				}
				dialog.show(null);
				return true;
			}
		});
		
		scrollButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int scroll = pref.loadInt(PREF_SCROLL_Y);
				mainScrollView.setScrollY(scroll);
			}
		});
		dateButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				final EditText editText = new EditText(context);
				editText.setText(dateStr);
				FragmentDialogClass dialog = new FragmentDialogClass(editText, activity);
				dialog.show("date", new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dateStr = editText.getText().toString();
						pref.saveString(getPrefDate(), dateStr);
						Toast.makeText(context, dateStr, Toast.LENGTH_SHORT).show();
						Intent intent = new Intent(context, viewerActivity);
						startActivity(intent);
						activity.finish();
					}
				});
			}
		});
		backButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				dateStr = dateChange(dateStr, periodField(), -periodValue());
				pref.saveString(getPrefDate(), dateStr);
				Toast.makeText(context, dateStr, Toast.LENGTH_SHORT).show();
				Intent intent = new Intent(context, viewerActivity);
				startActivity(intent);
				activity.finish();
				
			}
		});
		
		goButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(!progressDao.exists(dateStr)){
					progress.archiveDate = dateStr;
					Calendar cal = Calendar.getInstance();
					int year = cal.get(Calendar.YEAR);
					int month = cal.get(Calendar.MONTH) + 1;
					int day = cal.get(Calendar.DAY_OF_MONTH);
					progress.seeingDate =  year + "/" + month + "/" + day;
					progressDao.insert(progress);
					
					
				}
				
				dateStr = dateChange(dateStr, periodField(), periodValue());
				pref.saveString(getPrefDate(), dateStr);
				Toast.makeText(context, dateStr, Toast.LENGTH_SHORT).show();
				
				
				
				
				Intent intent = new Intent(context, viewerActivity);
				startActivity(intent);
				activity.finish();
			}
		});
		
		
		for(int rowDummy = 0; rowDummy < data.size(); rowDummy++) {
			final int row = rowDummy;
			
			titleCheckBoxes.get(row).setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if(isChecked) data.get(row).check = 1;
					else data.get(row).check = 0;
//					Toast.makeText(context, data.get(row).title, Toast.LENGTH_SHORT).show();
					class UpdateCheckThread extends Thread{
						public void run(){
							dao.update(data.get(row));
						}
					}
					UpdateCheckThread update = new UpdateCheckThread();
					update.start();
					
				}
			});
			
			detailsButtons.get(row).setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					DetailsDialogClass.show(data.get(row), activity);
				}
			});
		}
		
	}
	
	
	
	// using function
	
	private String dateChange(String date, int dayChanging){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		try {
			cal.setTime(sdf.parse(date));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cal.add(Calendar.DAY_OF_MONTH, dayChanging);
		
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return year + "/" + month + "/" + day;
	}
	
	private String dateChange(String date, int field, int value){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		try {
			cal.setTime(sdf.parse(date));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		cal.add(field, value);
		
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return year + "/" + month + "/" + day;
		
	}
	
	private Calendar setDate(String date){
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		try {
			cal.setTime(sdf.parse(date));
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return cal;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event){
		if(keyCode == KeyEvent.KEYCODE_BACK){
			Intent intent = new Intent(context, StartActivity.class);
			
			startActivity(intent);
			activity.finish();
		}
		
		return false;
	}
	
	@Override
	protected void onPause(){
		super.onPause();
		
		pref.saveInt(PREF_SCROLL_Y, mainScrollView.getScrollY());
	}
	
	
	
}