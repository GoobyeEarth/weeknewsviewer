package com.example.weeknewsviewer;


import java.security.Key;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import com.example.weeknewsviewer.parts.ProgressDaoClass;
import com.example.weeknewsviewer.parts.ProgressDataClass;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Layout;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import library.widget.ListDialogClass;
import library.widget.ListDialogClass.ClickListener;
import library.widget.ListViewClass;
import library.widget.ListViewClass.ChildViewListener;
import library.widget.ListViewClass.ItemClickListener;
import library.widget.PreferenceClass;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class ProgressActivity extends Activity implements KeyInterface{
	private Activity  activity= this;
	private Context context = this;
	
	private ListViewClass listView;
	private ProgressDaoClass dao;
	private List<ProgressDataClass> data;
	
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		controller();

		mainview();

		listener();
	}
	private void controller(){
		PreferenceClass pref = new PreferenceClass(context, PREFERENCE_FILE);
		String databaseName = pref.loadString(PREF_DATABASE);
		dao = new ProgressDaoClass(context, databaseName);
	}
	
	private void mainview(){
		listView = new ListViewClass(context);
		setContentView(listView);
		
		listView.setChildView(new ChildViewListener() {
			
			@Override
			public View setView(int position, View convertView) {
				LinearLayout linearLayout = new LinearLayout(context);
				linearLayout.setOrientation(LinearLayout.VERTICAL);
				
				
				TextView textView = new TextView(context);
				linearLayout.addView(textView);
				textView.setTextSize(10);
				int padding = 0;

				textView.setPadding(padding, padding, padding, padding);
				textView.setTag(0);
				
				
		
				return linearLayout;
			}
			
			@Override
			public void setTextInputter(int position, View convertView, String[] strArray) {
				TextView textView = (TextView) convertView.findViewWithTag(0);
				textView.setText(strArray[0]);
			}
		});
		listView.add(new String[]{"log"}, null);
		
		
		data = dao.findAll();
		
		
		
		for (final ProgressDataClass raw : data) {
			listView.add(new String[]{raw.seeingDate + "\t\t\t" + raw.archiveDate}, new ItemClickListener() {
				
				@Override
				public void onLongClick(int num, String[] textArray) {
					
				}
				
				@Override
				public void onClick(int num, String[] textArray, View view) {
					ListDialogClass listDialog = new ListDialogClass(activity, raw.archiveDate);
					listDialog.addItem("check = 0", new ClickListener() {
						
						@Override
						public void setProcess(int num, String str) {
							Intent intent = new Intent(context, ViewerActivity.class);
							PreferenceClass pref = new PreferenceClass(context, PREFERENCE_FILE);
							pref.saveString(PREF_CURRENT_DATE, raw.archiveDate);
							startActivity(intent);
							
						}
					});
					
					listDialog.addItem("check = 1", new ClickListener() {
						
						@Override
						public void setProcess(int num, String str) {
							Intent intent = new Intent(context, ViewerActivity.class);
							PreferenceClass pref = new PreferenceClass(context, PREFERENCE_FILE);
							pref.saveString(PREF_CURRENT_DATE_FOR_SELECTED, raw.archiveDate);
							startActivity(intent);
							
						}
					});
					
					listDialog.show(null);
				}
			});
		}
		listView.setData();
		listView.setOnItemClickListener(null);
		
	}

	private void listener(){
		
	}
	
	////////////////////////////////////////////////////////////////////////////
	
	

}
